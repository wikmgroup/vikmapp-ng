/************************ LICENCE ***************************
*     This file is part of <ViKM Vital-IT Knowledge Management web application>
*     Copyright (C) <2016> SIB Swiss Institute of Bioinformatics
*
*     This program is free software: you can redistribute it and/or modify
*     it under the terms of the GNU Affero General Public License as
*     published by the Free Software Foundation, either version 3 of the
*     License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Affero General Public License for more details.
*
*     You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>
*
*****************************************************************/


/*global angular*/
(function(){
	'use strict';

	/**
	* @ngdoc directive
	* @name vikmApp.directive:compareTo
	* @description used to compare to models. Used initially to compare password and confirm password
	* # compareTo
	*/
	angular.module('vikmApp')
	.directive('compareTo', compareTo)
	.directive('checkPassword', checkPassword);


	function  compareTo() {
		return {
			require: "ngModel",
			scope: {
				otherModelValue: "=compareTo"
			},
			link: function(scope, element, attributes, ngModel) {
				ngModel.$validators.compareTo = function(modelValue) {
					return modelValue == scope.otherModelValue;
				};

				scope.$watch("otherModelValue", function() {
					ngModel.$validate();
				});
			}
		};
	};

	function checkPassword(){
		return {
			require: 'ngModel',
			link: function(scope, element, attr, ngModel) {
				function pwValidation(value) {
					if (value.length >= 8) {
						ngModel.$setValidity('length', true);
					} else {
						ngModel.$setValidity('length', false);
					}
					if (value.match(/\d/)) {
						ngModel.$setValidity('numerical', true);
					} else {
						ngModel.$setValidity('numerical', false);
					}
					if (value.match(/[a-z]/)) {
						ngModel.$setValidity('lowercase', true);
					} else {
						ngModel.$setValidity('lowercase', false);
					}
					if (value.match(/[A-Z]/)) {
						ngModel.$setValidity('uppercase', true);
					} else {
						ngModel.$setValidity('uppercase', false);
					}
					return value;
				}
				ngModel.$parsers.push(pwValidation);
			}
		};
	};


})();
