/************************ LICENCE ***************************
*     This file is part of <ViKM Vital-IT Knowledge Management web application>
*     Copyright (C) <2016> SIB Swiss Institute of Bioinformatics
*
*     This program is free software: you can redistribute it and/or modify
*     it under the terms of the GNU Affero General Public License as
*     published by the Free Software Foundation, either version 3 of the
*     License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Affero General Public License for more details.
*
*     You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>
*
*****************************************************************/


'use strict';

/**
 * @ngdoc filter
 * @name vikmApp.filter:splitDot
 * @function
 * @description
 * # splitDot
 * Filter in the vikmApp. Adds a _<br>_ tag after each '. '
 */
angular.module('vikmApp')
  .filter('splitDot', splitDot)
  .filter('formatMimeType', formatMimeType);

  function splitDot() {
      return function (input) {
  		return input.replace(/\.\s+/g,".<br>");
      };
   }

   function formatMimeType(){
	   return function(type){
		   return type.replace('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',"Excel").replace("application/vnd.openxmlformats-officedocument.wordprocessingml.document",'Word').replace('text/plain','txt').replace('application/vnd.openxmlformats-officedocument.spre','Excel').replace('application/vnd.oasis.opendocument.spreadsheet','OpenDocument Spreadsheet').replace('application/vnd.oasis.opendocument.presentation',' Open Document Format');
	   }
   }
