/************************ LICENCE ***************************
*     This file is part of <ViKM Vital-IT Knowledge Management web application>
*     Copyright (C) <2016> SIB Swiss Institute of Bioinformatics
*
*     This program is free software: you can redistribute it and/or modify
*     it under the terms of the GNU Affero General Public License as
*     published by the Free Software Foundation, either version 3 of the
*     License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Affero General Public License for more details.
*
*     You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>
*
*****************************************************************/


/*jslint node: true, newcap: true */
/*global angular */


(function () {
	'use strict';

	angular
	.module('vikmApp')
	.factory('Authentication', Authentication);

	Authentication.$inject = ['$http', 'localStorageService', '$rootScope', '$timeout', 'User','Restangular', 'Base64', 'Project', 'toastr', '$route'];
	function Authentication($http, localStorageService, $rootScope, $timeout, User,Restangular, Base64, Project, toastr, $route) {
		var service = {
			currentUser: {}
		};

		service.login = login;
		service.setCredentials = setCredentials;
		service.clearCredentials = clearCredentials;
		service.getCredentials = getCredentials;
		service.setProject = setProject;
		service.getProject = getProject;

		return service;

		function login(username, password) {
			/* Use this for real authentication
			----------------------------------------------*/

			return Restangular.all('authenticate').post({username: username, password: password});

		}

		function setCredentials(user_id,username,authdata, permissions,group_id, project_id) {
			var permissionData = Base64.encode(permissions.join(";"));
			$http.defaults.headers.common['Authorization'] = 'Basic ' + authdata; // jshint ignore:line
			service.currentUser = {
				user_id: user_id,
				username: username,
				authdata: authdata,
				permissions: permissionData,
				group_id: group_id,
				project_id: project_id
			};
			service.currentUser.project = service.getProject();

			localStorageService.set('currentUser', service.currentUser);
		}

		function clearCredentials() {
			service.currentUser = {};
			localStorageService.remove('currentUser');
			$http.defaults.headers.common.Authorization = 'Basic';
		}

		function getCredentials() {
			var credentials = angular.copy(service.currentUser);
			if(service.currentUser.permissions !== undefined){
				var permissionData = Base64.decode(service.currentUser.permissions);
				credentials.permissions = permissionData.split(";");
			}
			return credentials;
		}

		function setProject(project_id) {
			Project.getOne(project_id).then(function(project){
				if(project.project_id){
					service.currentUser.project_id = project.project_id;
					service.currentUser.project = project;
					localStorageService.set('currentUser', service.currentUser);
					$route.reload();

				}
				else toastr.error('Permission denied','ERROR');
			});
		}

		function getProject(){
			var project_id = service.currentUser.project_id;
			var project;
			if(project_id){
				Project.getOne(project_id).then(function(data){
					project = data;
				});
			}
			return project;
		}
	}

})();