/************************ LICENCE ***************************
*     This file is part of <ViKM Vital-IT Knowledge Management web application>
*     Copyright (C) <2016> SIB Swiss Institute of Bioinformatics
*
*     This program is free software: you can redistribute it and/or modify
*     it under the terms of the GNU Affero General Public License as
*     published by the Free Software Foundation, either version 3 of the
*     License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Affero General Public License for more details.
*
*     You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>
*
*****************************************************************/

/*global angular*/

(function () {
	'use strict';
	angular.module('vikmApp').controller('ModalViewFileCtrl', function ($uibModalInstance, items, $sce,Authentication) {
		var vm = this;
		vm.usercode = Authentication.currentUser.authdata;
		vm.file_id = items.file_id;
		vm.indexfct = indexfct;
		vm.cancel = cancel;
		vm.urlviewer = '../../scripts/ViewerJS/#'+items.url;
		vm.url = items.url.replace(/\/index.php\/?/,'/');
		vm.sheet = 0;
		vm.tablecsv = items.tablecsv;
		vm.style = "table {color:#FA58AC}";
		if (vm.tablecsv.type === 'xls') {vm.myHtmlVar = vm.tablecsv.tables[vm.sheet]; }
		if (vm.tablecsv.type === 'md') {vm.myHtmlVar = vm.tablecsv.content;}
		vm.trustAsHtml = function (html) {
			return $sce.trustAsHtml(html);
		};

		vm.trustAsUrl=$sce.trustAsResourceUrl(vm.urlviewer);

		function indexfct(x){
			vm.sheet = x;
			vm.myHtmlVar= vm.tablecsv.tables[vm.sheet];
		}
		function cancel() {
			$uibModalInstance.dismiss('cancel');
		}
	});
})();
