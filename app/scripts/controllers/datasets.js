/************************ LICENCE ***************************
*     This file is part of <ViKM Vital-IT Knowledge Management web application>
*     Copyright (C) <2016> SIB Swiss Institute of Bioinformatics
*
*     This program is free software: you can redistribute it and/or modify
*     it under the terms of the GNU Affero General Public License as
*     published by the Free Software Foundation, either version 3 of the
*     License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Affero General Public License for more details.
*
*     You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>
*
*****************************************************************/
/*global angular */
(function(){
	'use strict';

	/**
	 * @ngdoc controller
	 * @name vikmApp.controller:ExperimentsCtrl
	 * @description
	 * # set active project based on project stored in Authentication service.
	 * Controller of the vikmApp
	 */
	angular.module('vikmApp')
	.controller('ExperimentsCtrl', ExperimentsCtrl);

	ExperimentsCtrl.$inject = ['$location', 'Restangular', 'Authentication', 'siteTitle','ExperimentList'];
	function ExperimentsCtrl($location, Restangular,Authentication, siteTitle, ExperimentList){
		var vm = this;
		vm.siteTitle = siteTitle.name;
		vm.datasets = ExperimentList;
		vm.project = Authentication.currentUser.project;
		vm.initExperiment = initExperiment;
		vm.goTo = goTo;

		//////////////

		/**
		* @ngdoc function
		* @name initExperiment
		* @description
		* # change path to display edit dataset form. Experiment ID = new
		*/
		function initExperiment(){
			$location.path('/dataset/new/edit');
		}

		/**
		* @ngdoc function
		* @name goTo
		* @param id
		* @description
		* # change path to display edit dataset form.
		*/

		function goTo(id){
			$location.path("/dataset/"+id);
		}

	}
})();

