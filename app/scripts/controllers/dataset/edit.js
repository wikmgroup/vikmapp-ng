/************************ LICENCE ***************************
*     This file is part of <ViKM Vital-IT Knowledge Management web application>
*     Copyright (C) <2016> SIB Swiss Institute of Bioinformatics
*
*     This program is free software: you can redistribute it and/or modify
*     it under the terms of the GNU Affero General Public License as
*     published by the Free Software Foundation, either version 3 of the
*     License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Affero General Public License for more details.
*
*     You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>
*
*****************************************************************/


/*global angular*/
(function(){
	'use strict';

	/**
	 * @ngdoc controller
	 * @name vikmApp.controller:ExperimentExperimentIdCtrl
	 * @description
	 * # Edit dataset controller
	 * Controller of the vikmApp
	 */
	angular.module('vikmApp')
	.controller('ExperimentEditCtrl', ExperimentEditCtrl);

	ExperimentEditCtrl.$inject = ['$routeParams','Experiments', 'User','permissions','toastr','$location','Authentication'];
	function ExperimentEditCtrl($routeParams,Experiments, User, permissions, toastr, $location, Authentication){
		var vm = this;
		vm.currentProjectId = Authentication.currentUser.project_id;
		vm.project = Authentication.currentUser.project;
		vm.submitExperiment = submitExperiment;

		var datasetId = $routeParams.dataset_id;
		vm.permissions = permissions;
		User.GetByAuthdata(Authentication.currentUser.authdata).then(function(data){
			var loggedUser = data;
			vm.groups = loggedUser.groups;
			if(datasetId == 'new'){
				vm.dataset = {
					dataset_id: -1,
					name: '',
					description: '',
					group_permissions: 2,
					project_permissions: 1,
					public_permissions: 0,
					request_download: 'Y',
					group_id: loggedUser.groups[0].group_id,
					group_name: loggedUser.groups[0].name,
					user_id: loggedUser.user_id,
					user_name: loggedUser.firstname+" "+loggedUser.lastname,
					project_id: vm.project.project_id,
					project_name: vm.project.name
				};
			}
			else{
				Experiments.getOne(datasetId).then(function(dataset){
					vm.dataset = dataset;
				});
			}


		});



		///////
		function submitExperiment(){
			if(vm.dataset.dataset_id == -1) Experiments.dataset.post(vm.dataset).then(function(dataset){
				toastr.success('Dataset created successfuly','success');
				console.log(dataset)
				$location.path("/dataset/"+dataset.dataset_id);
			});
			else vm.dataset.save().then(function(dataset){
				toastr.success('Dataset updated successfuly','success');
				$location.path("/dataset/"+dataset.dataset_id);

			});
		}

	}
})();
