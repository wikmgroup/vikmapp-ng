/************************ LICENCE ***************************
*     This file is part of <ViKM Vital-IT Knowledge Management web application>
*     Copyright (C) <2016> SIB Swiss Institute of Bioinformatics
*
*     This program is free software: you can redistribute it and/or modify
*     it under the terms of the GNU Affero General Public License as
*     published by the Free Software Foundation, either version 3 of the
*     License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Affero General Public License for more details.
*
*     You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>
*
*****************************************************************/


/*global angular*/
(function(){
	'use strict';
	angular.module('libs', [])
	.service('_', function ($window) {
		return $window._;
	})
	.service('moment', function ($window) {
		return $window.moment;
	})
	.service('visavailChart', function ($window) {
		return $window.visavailChart;
	})
	/**
	* provide d3v3 as 'd3v3'
	*/
	.factory('d3v3', ['$document', '$q', '$rootScope','$window',
	function($document, $q, $rootScope,$window) {
		var d = $q.defer();
		function onScriptLoad() {
			// Load client in the browser
			$window.d3v3 = window.d3;
			$window.d3 = undefined;
			window.d3 = undefined;
			$rootScope.$apply(function() { d.resolve($window.d3v3); });
		}
		// Create a script tag with d3 as the source
		// and call our onScriptLoad callback when it
		// has been loaded
		var scriptTag = $document[0].createElement('script');
		scriptTag.type = 'text/javascript';
		scriptTag.async = true;
		scriptTag.src = 'https://d3js.org/d3.v3.min.js';
		scriptTag.onreadystatechange = function () {
			if (this.readyState == 'complete') onScriptLoad();
		}
		scriptTag.onload = onScriptLoad;

		var s = $document[0].getElementsByTagName('body')[0];
		s.appendChild(scriptTag);

		return {
			d3: function() { return d.promise; }
		};
	}])

})();
