/************************ LICENCE ***************************
*     This file is part of <ViKM Vital-IT Knowledge Management web application>
*     Copyright (C) <2016> SIB Swiss Institute of Bioinformatics
*
*     This program is free software: you can redistribute it and/or modify
*     it under the terms of the GNU Affero General Public License as
*     published by the Free Software Foundation, either version 3 of the
*     License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Affero General Public License for more details.
*
*     You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>
*
*****************************************************************/


/*global angular, console, self*/
(function () {
	'use strict';


	/**
	* @ngdoc controller
	* @name vikmApp.controller:ExperimentExperimentIdCtrl
	* @description
	* # ExperimentExperimentIdCtrl
	* Controller of the vikmApp
	*/
	angular.module('vikmApp')
	.controller('ExperimentViewCtrl', ExperimentViewCtrl);

	ExperimentViewCtrl.$inject = ['$timeout','datasetData','permissions','FileUploader','ENV','Authentication','toastr','Restangular','$location','$http','$uibModal','_'];
	function ExperimentViewCtrl($timeout,datasetData,permissions,FileUploader, ENV, Authentication, toastr, Restangular,$location,$http,$uibModal,_){
		var vm = this;
		if(+datasetData.project_id !== +Authentication.currentUser.project_id) $location.path("/datasets");
		vm.dataset = datasetData;
		vm.dataset.files = vm.dataset.files.map(function(f){f.selected = false; return f;});

		vm.permissions = {};
		angular.forEach(permissions,function(perm){
			vm.permissions[perm.id] = perm.name;
		});
		vm.showUpload = false;
		vm.uploader = new FileUploader({});
		vm.deleteFile = deleteFile;
		vm.download = download;
		vm.confirmDeletion = false;
		vm.deleteExperiment = deleteExperiment;
		vm.editExperiment = editExperiment;
		vm.viewFile = viewFile;
		vm.toggleFile = toggleFile;
		vm.fail = fail;
		vm.success = success;
		vm.geturl = geturl ;
		vm.file_id_array =[];
		vm.itemsByPages=10;
		vm.itemsValue = [5,10,25,50];
		if(ENV.serverURL.indexOf("http") >= 0){
			vm.url = ENV.serverURL;
		}
		else{
			vm.url = $location.protocol()+'://'+$location.host()+'/api/index.php';
		}


		var uploader = vm.uploader = new FileUploader({
			headers:{
				'Authorization': 'Basic ' + Authentication.currentUser.authdata
			},
			url: ENV.serverURL+'/dataset/'+vm.dataset.dataset_id+"/upload"
		});
		vm.filetype = ["image/jpeg","image/png", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","text/plain","application/pdf","application/vnd.oasis.opendocument.spreadsheet","application/vnd.oasis.opendocument.presentation","application/vnd.openxmlformats-officedocument.spre"];
		uploader.onSuccessItem = function(fileItem, response) {
			toastr.success('file ' + fileItem.file.name + ' has been uploaded','Success');
			vm.dataset.files = response.files;
			vm.dataset.readme = response.readme;
		};
		uploader.onCompleteAll = function() {
			vm.showUpload = false;
		};
		function deleteFile(file_id){
			Restangular.one('file',file_id).remove().then(function(data){
				vm.dataset.files = data.files;
				vm.dataset.readme = data.readme;
			});
		}

		function download(file_id){
			if(!file_id){
				var files = [];
				angular.forEach(vm.dataset.files,function(f){
					if(f.selected) files.push(f.file_id);
				});
				file_id = files.join(',');
			}
			Restangular.one('dataset',vm.dataset.dataset_id).one('file',file_id).get().then(function(data){
				if(data.path) self.location.href = ENV.serverURL+"/file/"+data.path;
				if(data.message) toastr.info(data.message,'FILE');
				if(data.requested) vm.dataset.logged_request_download = 'P';
			});
		}

		function deleteExperiment(){
			if(!vm.confirmDeletion) vm.confirmDeletion = !vm.confirmDeletion;
			else{
				Restangular.one('dataset',vm.dataset.dataset_id).remove().then(function(){
					$location.path("/datasets");
					toastr.success('Dataset deleted successfuly','SUCCESS');
				});
			}
		}

		function editExperiment(){
			if(vm.dataset.dataset_id > -1) $location.path("dataset/"+vm.dataset.dataset_id+"/edit");
		}

		function viewFile(file_id,access_code,index,mime_type,filename){
			vm.file_id = file_id;
			vm.wait = true;
			var file_url = ENV.serverURL;
			var execbyviewer = ["application/pdf","application/vnd.oasis.opendocument.spreadsheet","application/vnd.oasis.opendocument.presentation","application/vnd.openxmlformats-officedocument.spre"];
			if(_.includes(execbyviewer, mime_type)){
				file_url =  vm.url +'/download/'+vm.dataset.dataset_id+'/'+file_id+'/'+access_code+'/'+filename;

			}


			Restangular.one('dataset',vm.dataset.dataset_id).one('file',file_id).one('view').get().then(function(data){
				vm.wait = false;

				vm.tablecsv = data;
				$uibModal.open({
					animation: vm.animationsEnabled,
					templateUrl: 'views/modal/modalViewFile.html',
					controller: 'ModalViewFileCtrl',
					controllerAs: 'vm',
					size: "lg",
					resolve: {
						items: function () {
						vm.table ={
							tablecsv: vm.tablecsv,
							url:file_url,
							file_id:file_id
						};
						return vm.table;}
					}
				});
			});
		}

		function toggleFile(file_id){
			angular.forEach(vm.dataset.files,function(file,idx){
				// if(file.file_id == file_id) vm.dataset.files[idx].selected = !vm.dataset.files[idx].selected;
			});
		}

		function success(filename){
			toastr.success('Url copied to clipboard for '+filename,'SUCCESS');		}

		function fail(filename){
			toastr.error('Oups, something went wrong. Failed to create a link for '+filename,'FAILED');
		}

		function geturl(file_id,access_code,index,filename){
			angular.forEach(vm.dataset.files,function(file){

				if(file.file_id != vm.dataset.files[index].file_id){
					file.highlight = false;
				}


			});

			vm.dataset.files[index].url= vm.url +'/download/'+vm.dataset.dataset_id+'/'+file_id+'/'+access_code+'/'+filename;


		}

	}
})();
