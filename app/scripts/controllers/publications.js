/************************ LICENCE ***************************
*     This file is part of <ViKM Vital-IT Knowledge Management web application>
*     Copyright (C) <2016> SIB Swiss Institute of Bioinformatics
*
*     This program is free software: you can redistribute it and/or modify
*     it under the terms of the GNU Affero General Public License as
*     published by the Free Software Foundation, either version 3 of the
*     License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Affero General Public License for more details.
*
*     You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>
*
*****************************************************************/


/*global angular*/
(function(){
	'use strict';

	/**
	 * @ngdoc controller
	 * @name vikmApp.controller:PublicationsCtrl
	 * @description
	 * # PublicationsCtrl
	 * Controller of the vikmApp
	 * list publications of the app
	 */
	angular.module('vikmApp')
	.controller('PublicationsCtrl', PublicationsCtrl);

	PublicationsCtrl.$inject = ['$location', 'Restangular', 'Authentication', 'siteTitle','publications','$timeout','toastr'];
	function PublicationsCtrl($location, Restangular,Authentication, siteTitle,publications,$timeout,toastr){
		var vm = this;
		vm.siteTitle = siteTitle;
		vm.publications = publications;
		vm.filtered_publications = angular.copy(publications);
		vm.currentUser = Authentication.currentUser;
		
		vm.cancel = cancel;
		vm.save = save;
		vm.edit = edit;
		vm.remove = remove;
		vm.reset = reset;
		vm.getPubmed = getPubmed;
		vm.testExistence = testExistence;
		vm.applyFilters = applyFilters;
		vm.resetFilters = resetFilters;
			
		vm.new = {};
		vm.previous={};
		vm.wait = {pubmed:false};
		vm.filters = {$display:false,title:'', journal:[], year:[], doi:[], pmid:[],authors:[]};
		vm.fields = {doi:[],year:[],journal:[],pmid:[],authors:[]};
		vm.status = (vm.publications.length) ? 'display':'edit';

		getListOfFields();
		
		
		// F ====== RESET FILTERS ==========
		function resetFilters(){
			vm.filters = {$display:true,title:'', journal:[], year:[], doi:[], pmid:[],authors:[]};
			vm.filtered_publications = angular.copy(vm.publications);
		}
		
		// F ====== APPLY FILTERS ==========
		function applyFilters(){
			vm.filtered_publications = [];
			_.forEach(vm.publications,function(p){
				var authors = _.map(p.authors.split(','),function(a){return a.trim()});
				var keep = true;
				_.forEach(vm.filters,function(f,i){
					if(!i.startsWith('$')){
						if(i=='title'){if(f && p.title.toLowerCase().indexOf(f.toLowerCase())<0){keep = false; }}
						else if(i=='authors'){
							if(f.length){								
								var keepAuthors = false;
								_.forEach(f,function(fa){ if(_.includes(authors,fa)){keepAuthors = true;} });
								if(!keepAuthors){keep = false;}
							}
						}
						else if(f.length && !_.includes(f,p[i])){ keep = false; }
					}
				});
				if(keep){ vm.filtered_publications.push(p); }
			});
		}
				
		// F ====== GET LIST OF FIELDS ==========
		function getListOfFields(){
			vm.fields = {doi:[],year:[],journal:[],pmid:[],authors:[]};
			_.forEach(vm.publications,function(p){
				var authors = _.map(p.authors.split(','),function(a){return a.trim()});
				_.forEach(authors,function(a){
					if(!_.includes(vm.fields.authors,a)){vm.fields.authors.push(a);}
				});
				if(!_.includes(vm.fields.doi,p.doi)){vm.fields.doi.push(p.doi);}
				if(!_.includes(vm.fields.pmid,p.pmid)){vm.fields.pmid.push(p.pmid);}
				if(!_.includes(vm.fields.journal,p.journal)){vm.fields.journal.push(p.journal);}
				if(!_.includes(vm.fields.year,p.year)){vm.fields.year.push(p.year);}
			});
		} 

		// F ====== CANCEL EDIT ==========
		function cancel(){
			if(vm.new.publication_id){
				var pidx = _.findIndex(vm.publications,function(p){return p.publication_id == vm.new.publication_id;});
				if(pidx>-1){ vm.publications[pidx] = angular.copy(vm.previous); }
			}
			else{
				vm.new = {};				
			}
			applyFilters();
			vm.status = 'display';
		} 
		
		// F ====== SAVE PUBLICATION ==========
		function save(){
			if(!testExistence(vm.new)){
				if(vm.new.publication_id){
					vm.new.put().then(function(data){
						if(data.publication_id){
							getListOfFields();
							applyFilters();
							vm.status = 'display';
						}
					});
				}
				else{
					vm.publications.post(vm.new).then(function(data){
						if(data.publication_id){
							vm.publications.push(data);
							getListOfFields();
							vm.new = {};
							applyFilters();
						}
						vm.status = 'display';
					})
				}
			}
		}

		// F ====== EDIT PUBLICATION ==========
		function edit(publi){
			if(publi){				
				var pidx = _.findIndex(vm.publications,function(p){return p.publication_id == publi.publication_id;});
				if(pidx>-1){
					vm.previous = angular.copy(vm.publications[pidx]);
					vm.new = vm.publications[pidx];
				}
			}
			else{
				vm.new = {year:new Date().getFullYear()};
			}
			vm.status = 'edit';
		}
		
		// F ====== REMOVE PUBLICATION ==========
		function remove(publi){
			Restangular.one('publication',publi.publication_id).remove().then(function(data){
				var publiIdx = _.findIndex(vm.publications,function(p){return p.publication_id == data});
				if(publiIdx>-1){
					vm.publications.splice(publiIdx,1);					
					publi.confirm_deletion = false;
					getListOfFields();
				}
				applyFilters();
			});
		}
		
		// F ====== RESET FORM ==========
		function reset(){
			vm.new = {};
		}
		
		// F ====== TEST EXITENCE ==========
		function testExistence(publi){
			var obj,supp = '';
			var pidx = _.findIndex(vm.publications,function(p){
				obj = null;
				if(!publi.publication_id || (publi.publication_id && publi.publication_id != p.publication_id)){					
					if(publi.pmid && p.pmid && p.pmid.trim()==publi.pmid.trim()){ obj = 'pmid'; }
					// else if(publi.doi && p.doi && p.doi.trim()==publi.doi.trim()){ obj = 'doi'; }
					else if(publi.title && p.title.toLowerCase().trim().indexOf(publi.title.toLowerCase().trim())>-1){ obj = 'title';supp='<br>Please check on the list or add doi or pmid to confirm.'; }
				}
				if(obj)return true;
				return false;
			});
			if(pidx>-1){
				toastr.warning('This publication ('+obj+'='+vm.new[obj]+') is already in the list.' + supp);
				return true;
			}
			else{
				return false;
			}
		}
		
		// F ====== GET FROM PUBMED ==========
		var getPubmedTimeout = null;
		function getPubmed(){
			if(getPubmedTimeout){ $timeout.cancel(getPubmedTimeout);}
			getPubmedTimeout = $timeout(function(){
				if(vm.new.pmid && !testExistence(vm.new)){
					if(Number.isInteger(+vm.new.pmid)){
						vm.wait.pubmed = true;
						Restangular.one('pubmed',vm.new.pmid).get().then(function(data){
							vm.wait.pubmed = false;
							if(data && data.pmid){
								vm.new.title = data.title;
								vm.new.authors = data.authors;
								vm.new.journal = data.journal;
								vm.new.year = data.year;
								vm.new.volume = data.volume;
								vm.new.page = data.page;
								vm.new.doi = data.doi;
								vm.new.url = (data.doi) ? 'https://doi.org/' + data.doi : 'https://www.ncbi.nlm.nih.gov/pubmed/'+vm.new.pmid;
								vm.new.abstract = data.abstract;
							}
							else{
								toastr.info('Pubmed n°'+vm.new.pmid+ ' doesn\'t exist.');
							}
						});
					}
					else{
						toastr.info('Pubmed ID should be a number.');
					}
				}
			},700);
		};

	}
})();

