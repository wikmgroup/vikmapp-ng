/************************ LICENCE ***************************
*     This file is part of <ViKM Vital-IT Knowledge Management web application>
*     Copyright (C) <2016> SIB Swiss Institute of Bioinformatics
*
*     This program is free software: you can redistribute it and/or modify
*     it under the terms of the GNU Affero General Public License as
*     published by the Free Software Foundation, either version 3 of the
*     License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Affero General Public License for more details.
*
*     You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>
*
*****************************************************************/


/*global angular*/
(function(){
	'use strict';

	/**
	 * @ngdoc service
	 * @name vikmApp.Project
	 * @description
	 * # Project
	 * Factory in the vikmApp. Gets a list or a project
	 */
	angular.module('vikmApp')
	.factory('Project', ProjectService);

	ProjectService.$inject = ['Restangular'];
	function ProjectService(Restangular){
		var service = {};
		service.projects = Restangular.all('project');
		service.getAll = getAll;
		service.getOne = getOne;
		return service;


		///////

		function getAll(){
			return service.projects.getList().then(function(data){
				return data;
			});
		}


		function getOne(project_id){
			return Restangular.one('project',project_id).get().then(function(data){
				return data;
			});
		}
	}

})();
